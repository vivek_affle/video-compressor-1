ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

* .metadata/
* .metadata/.lock
* .metadata/.mylyn/
* .metadata/.mylyn/.taskListIndex/
* .metadata/.mylyn/.taskListIndex/segments.gen
* .metadata/.mylyn/.taskListIndex/segments_1
* .metadata/.plugins/
* .metadata/.plugins/org.eclipse.cdt.core/
* .metadata/.plugins/org.eclipse.cdt.core/.log
* .metadata/.plugins/org.eclipse.cdt.make.core/
* .metadata/.plugins/org.eclipse.cdt.make.core/specs.c
* .metadata/.plugins/org.eclipse.cdt.make.core/specs.cpp
* .metadata/.plugins/org.eclipse.core.resources/
* .metadata/.plugins/org.eclipse.core.resources/.root/
* .metadata/.plugins/org.eclipse.core.resources/.root/.indexes/
* .metadata/.plugins/org.eclipse.core.resources/.root/.indexes/properties.index
* .metadata/.plugins/org.eclipse.core.runtime/
* .metadata/.plugins/org.eclipse.core.runtime/.settings/
* .metadata/.plugins/org.eclipse.core.runtime/.settings/org.eclipse.jdt.ui.prefs
* .metadata/.plugins/org.eclipse.core.runtime/.settings/org.eclipse.mylyn.context.core.prefs
* .metadata/.plugins/org.eclipse.core.runtime/.settings/org.eclipse.mylyn.monitor.ui.prefs
* .metadata/.plugins/org.eclipse.core.runtime/.settings/org.eclipse.mylyn.tasks.ui.prefs
* .metadata/.plugins/org.eclipse.core.runtime/.settings/org.eclipse.ui.prefs
* .metadata/.plugins/org.eclipse.m2e.logback.configuration/
* .metadata/.plugins/org.eclipse.m2e.logback.configuration/0.log
* .metadata/.plugins/org.eclipse.m2e.logback.configuration/logback.1.5.1.20150109-1820.xml
* .metadata/.plugins/org.eclipse.rse.core/
* .metadata/.plugins/org.eclipse.rse.core/.log
* .metadata/.plugins/org.eclipse.rse.core/initializerMarks/
* .metadata/.plugins/org.eclipse.rse.core/initializerMarks/org.eclipse.rse.internal.core.RSELocalConnectionInitializer.mark
* .metadata/.plugins/org.eclipse.team.cvs.core/
* .metadata/.plugins/org.eclipse.team.cvs.core/.running
* .metadata/.plugins/org.eclipse.ui.workbench/
* .metadata/.plugins/org.eclipse.ui.workbench/workingsets.xml
* .metadata/version.ini
* ic_launcher-web.png
* proguard-project.txt

Replaced Jars with Dependencies:
--------------------------------
The importer recognized the following .jar files as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the .jar file in your project was of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the jar replacement in the import wizard and try again:

android-support-v4.jar => com.android.support:support-v4:21.0.3
android-support-v7-appcompat.jar => com.android.support:appcompat-v7:21.0.3

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

* AndroidManifest.xml => app/src/main/AndroidManifest.xml
* assets/ => app/src/main/assets
* jni/ => app/src/main/jni/
* libs/android-support-v7-recyclerview.jar => app/libs/android-support-v7-recyclerview.jar
* libs/armeabi/libCompressor.so => app/src/main/jniLibs/armeabi/libCompressor.so
* libs/armeabi/libavcodec-56.so => app/src/main/jniLibs/armeabi/libavcodec-56.so
* libs/armeabi/libavfilter-5.so => app/src/main/jniLibs/armeabi/libavfilter-5.so
* libs/armeabi/libavformat-56.so => app/src/main/jniLibs/armeabi/libavformat-56.so
* libs/armeabi/libavutil-54.so => app/src/main/jniLibs/armeabi/libavutil-54.so
* libs/armeabi/libpostproc-53.so => app/src/main/jniLibs/armeabi/libpostproc-53.so
* libs/armeabi/libswresample-1.so => app/src/main/jniLibs/armeabi/libswresample-1.so
* libs/armeabi/libswscale-3.so => app/src/main/jniLibs/armeabi/libswscale-3.so
* lint.xml => app/lint.xml
* res/ => app/src/main/res/
* src/ => app/src/main/java/

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
