package com.lkland.videocompressor.responsemanager;

import android.app.Activity;
import android.app.Notification;
import android.widget.TextView;

import com.lkland.util.Logger;
import com.lkland.videocompressor.R;
import com.lkland.videocompressor.compressor.ICompressor;
import com.lkland.videocompressor.fragments.QueueListFragment;
import com.lkland.videocompressor.parser.ProgressPaser;
import com.lkland.videocompressor.services.AbstractCompressionService;
import com.lkland.videocompressor.video.IVideo;
import com.lkland.videocompressor.workqueue.IQueueable;

public class ActivityResponseManager extends AbstractResponseManager{
	protected QueueListFragment mFragment;
	public ActivityResponseManager(QueueListFragment fragment) {
		this.mFragment = fragment;	
	}

	@Override
	public void onProgress(IVideo v, final String str) {
		
		if(mFragment!=null)
			mFragment.progress(v, str);
	}

	@Override
	public void onQueueFinished() {
		if(mFragment!=null)
			mFragment.queueFinished();
	}

	@Override
	public void onPop(IVideo v) {
		if(mFragment!=null)
			mFragment.remove(v);
	}

	@Override
	public void onQueueStart() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onRemove(IVideo v) {
		if(mFragment!=null)
			mFragment.remove(v);
	}

	@Override
	public void onAdd(IVideo v) {
		if(mFragment!=null)
			mFragment.add(v);
	}

}
